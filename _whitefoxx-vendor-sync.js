var childProcess = require('child_process');
var CredentialsManager = require('./bin/credentials-manager').CredentialsManager;
var fs = require('fs');
var request = require('request');


/*******************************************************************************
 *
 * bigbuy
 *
 ******************************************************************************/

var BigBuy = function () {
    var that = this;

    var cookiesFilePath = './etc/cookies/bigbuy.json';
    this.cookiesManager = require('./bin/cookies-manager')(cookiesFilePath);
    this.cookies = null;
    setLocalCookies();

    var credentialsFilePath = './etc/credentials/bigbuy.json';
    //this.credentialsManager = require('./bin/credentials-manager')(credentialsFilePath);
    this.credentialsManager = new CredentialsManager(credentialsFilePath);
    this.username = null;
    this.password = null;
    this.passwordPlaceholder = null;
    setLocalCredentials();

    this.synced = false;

    BigBuy.prototype.sync = function (credentials, cb) {
        console.log('setting credentials');
        that.credentialsManager.set(credentials);
        setLocalCredentials();
        console.log('credentials set');
        console.log(this.username);
        console.log(this.password);

        getCookies(function (cookies) {
            that.cookiesManager.set(cookies);
            setLocalCookies();
            attemptSync(function (synced) {
                that.synced = synced;
                console.log('bigbuy synced: ' + that.synced);
                cb(that.synced);
            });
        });
    };

    function setLocalCookies() {
        that.cookies = that.cookiesManager.cookies;
    }

    function setLocalCredentials() {
        that.username = that.credentialsManager.username;
        that.password = that.credentialsManager.password;
        console.log(that.username);
        console.log(that.password);
        if (that.password) {
            that.passwordPlaceholder = '';
            for (var i = 0; i < that.password.length; i++) {
                that.passwordPlaceholder += '*';
            }
        }
    }

    function getCookies(cb) {
        var getCookiesCmd =
            'casperjs ./bin/bigbuy/get-cookies.js' + ' ' +
            '--username=' + that.username + ' ' +
            '--password=' + that.password;
        childProcess.exec(getCookiesCmd, function (err, cookies) {
            cb(cookies);
        });
    };

    function attemptSync(cb) {
        var attemptSyncCmd =
            'casperjs ./bin/bigbuy/attempt-sync.js' + ' ' +
            '--cookies-file-path=' + that.cookiesManager.cookiesFilePath;
        childProcess.exec(attemptSyncCmd, function (err, result) {
            result = JSON.parse(result);
            cb(result.synced);
        });
    }
};

/******************************************************************************/


/*******************************************************************************
 *
 * chinavasion.
 *
 ******************************************************************************/

var Chinavasion = function () {
    var that = this;

    var credentialsFilePath = './etc/credentials/chinavasion.json';
//     this.credentialsManager = require('./bin/credentials-manager')(credentialsFilePath);
    this.credentialsManager = new CredentialsManager(credentialsFilePath);
    this.apiKey = null;
    this.apiKeyPlaceholder = null;
    setLocalCredentials();

    this.synced = false;

    Chinavasion.prototype.sync = function (credentials, cb) {
        this.credentialsManager.set(credentials);
        setLocalCredentials();

        var req = {
            method: 'GET',
            url: 'https://secure.chinavasion.com/api/getCategory.php',
            json: {
                "key": that.apiKey,
                "include_content": "0"
            }
        };
        request(req, function (err, res, body) {
            if (body.error) {
                that.synced = false;
            } else {
                that.synced = true;
            }
            cb(that.synced);
        });
    };

    if (this.apiKey) {
        var credentials = {
            apiKey: this.apiKey
        };
        this.sync(credentials, function (synced) {
            that.synced = synced;
        });
    }

    function setLocalCredentials() {
        that.apiKey = that.credentialsManager.apiKey;
        if (that.apiKey) {
            that.apiKeyPlaceholder = '';
            for (var i = 0; i < that.apiKey.length; i++) {
                that.apiKeyPlaceholder += '*';
            }
        }
    }
// 
//     Chinavasion.prototype.getProductStockInfo = function (sku, quantity, cb) {
//         var req = {
//             method: 'GET',
//             uri: 'https://secure.chinavasion.com/api/getPrice.php',
//             json: {
//                 "key": this.apiKey,
//                 "currency": "GBP",
//                 "socket": "UK",
//                 "products": [
//                     {
//                         "model_code": sku,
//                         "quantity": quantity
//                     }
//                 ],
//                 "shipping_country_iso2": "GB"
//             }
//         };
//         request(req, function (err, res, body) {
//             var shipping = body.shipping;
//             if (shipping.length) {
//                 cb(true);
//             } else {
//                 cb(false);
//             }
//         });
//     };
// 
//     // if not synced todo
//     Chinavasion.prototype.getProductShippingInfo = function (countryCode, sku, quantity, cb) {
//         var req = {
//             method: 'GET',
//             uri: 'https://secure.chinavasion.com/api/getPrice.php',
//             json: {
//                 "key": this.apiKey,
//                 "currency": "GBP",
//                 "socket": "UK",
//                 "products": [
//                     {
//                         "model_code": sku,
//                         "quantity": quantity
//                     }
//                 ],
//                 "shipping_country_iso2": "GB"
//             }
//         };
//         request(req, function (err, res, body) {
//             var shipping = body.shipping;
//             var shippingInfo = [];
//             shipping.forEach(function (shipping) {
//                 var service = shipping.name;
//                 var time = shipping.delivery;
//                 var cost = shipping.price;
//                 shippingInfo.push({
//                     service: service,
//                     time: time,
//                     cost: cost
//                 });
//             });
//             cb(shippingInfo);
//         });
//     };
};

/******************************************************************************/

exports.BigBuy = new BigBuy();
exports.Chinavasion = new Chinavasion();
// exports.DealExtreme = new DealExtreme();
// exports.EPetDiscount = new EPetDiscount();
// exports.HJCloseouts = new HJCloseouts();
// exports.TinyDeal = new TinyDeal();
