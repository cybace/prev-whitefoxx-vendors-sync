var fs = require('fs');

var CredentialsManager = function (credentialsFilePath) {
    var that = this;

    this.credentialsFilePath = credentialsFilePath;
    if (!fs.existsSync(this.credentialsFilePath)) {
        fs.openSync(this.credentialsFilePath, 'w');
    }

    this.apiKey = null;
    this.username = null;
    this.password = null;
    setLocalCredentials();

    CredentialsManager.prototype.set = function (credentials) {
        fs.writeFileSync(this.credentialsFilePath, JSON.stringify(credentials, null, 4));
        setLocalCredentials(this.credentialsFilePath);
    };

    function setLocalCredentials(fp) {
        if (fs.existsSync(fp)) {
            var data = fs.readFileSync(fp);
            try {
                var credentials = JSON.parse(data);
                console.log(fp);
                console.log(JSON.stringify(credentials, null, 4));
                if (credentials.apiKey) {
                    that.apiKey = credentials.apiKey;
                }
                if (credentials.username) {
                    that.username = credentials.username;
                }
                if (credentials.password) {
                    that.password = credentials.password;
                }
            } catch (e) {
                that.apiKey = null;
                that.username = null;
                that.password = null;
            }
        }
    }
};

exports.CredentialsManager = CredentialsManager;
