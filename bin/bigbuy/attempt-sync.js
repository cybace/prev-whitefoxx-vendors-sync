var casper = require('casper').create();
var fs = require('fs');

var cookiesFilePath = casper.cli.get('cookies-file-path');
var cookies = JSON.parse(JSON.parse(fs.read(cookiesFilePath)));
phantom.clearCookies();
cookies.forEach(function (cookie) {
    phantom.addCookie(cookie);
});

var URI = 'https://www.bigbuy.eu/en/controlpanel/order';

casper.start().thenOpen(URI);

casper.waitForSelector('.panel-title', function () {
    console.log(JSON.stringify({ synced: 1 }));
}, function () {
    console.log(JSON.stringify({ synced: 0 }));
});

casper.run();
