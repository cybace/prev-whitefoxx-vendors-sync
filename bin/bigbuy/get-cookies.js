var casper = require('casper').create();

var username = '' + casper.cli.get('username');
var password = '' + casper.cli.get('password');

var URI = 'https://www.bigbuy.eu/en/account/login';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form.form input#email', username);
    this.sendKeys('form.form input#passwd', password);
    this.click('button.button.button--primary.button--big.u-mgt--half.u-flr');
});

casper.waitWhileSelector('form.form input#passwd', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
});

casper.run();
