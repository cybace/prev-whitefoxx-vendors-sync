var casper = require('casper').create();
var x = require('casper').selectXPath;

var sku = '' + casper.cli.get('sku');
var quantity = '' + casper.cli.get('quantity');
var countryCode = '' + casper.cli.get('countryCode');
var postCode = '' + casper.cli.get('postCode');

var URI = 'https://www.bigbuy.eu/en/search/result?q=' + sku;

casper.start().thenOpen(URI);

casper.waitForSelector('.productCard-link', function () {
    this.click('.productCard-link');
}, function () {
    casper.exit();
});

casper.waitForSelector('.form-input.form-input--postfix.unitControl-input', function () {
    this.sendKeys('.form-input.form-input--postfix.unitControl-input', '' + quantity);
    this.click('button.button--full.button--primary');
}, function () {
    casper.exit();
});

casper.waitForSelector('.expresscart', function () {
    this.click(x('//*[@id="login"]/button[3]'));
}, function () {
    casper.exit();
});

casper.waitForSelector('.expresscart.expresscart--show', function () {
    var countrySelector = 'select[data-js-shipping-cost="country"]';
    this.evaluate(function (countrySelector, countryCode) {
        var select = document.querySelector(countrySelector);
        Array.prototype.forEach.call(select.children, function (option, i) {
            if (option.value.indexOf(countryCode) !== -1) {
                select.selectedIndex = i;
            }
        });
        var e = document.createEvent('UIEvents');
        e.initUIEvent('change', true, true);
        select.dispatchEvent(e);
    }, countrySelector, countryCode);
    this.sendKeys('input[data-js-shipping-cost="cp"]', postCode);
    this.click('button[data-js-shipping-cost="submit"]');
}, function () {
    casper.exit();
});

casper.waitForSelector('.expresscart-scrollSection', function () {
    var shippingDataSelector = 'tbody[data-js-carrier_list]';
    var productShippingInfo = this.evaluate(function (shippingDataSelector) {
        var shippingData = document.querySelector(shippingDataSelector);
        var shippingOptions = shippingData.querySelectorAll('.table-tr');
        return Array.prototype.map.call(shippingOptions, function (shippingOption) {
            var shippingOptionData = shippingOption.querySelectorAll('td');
            var label = shippingOption.querySelector('.form-label.form-label--inline.u-mgl--3rd');
            var labelFor = label.htmlFor;
            var shippingService = labelFor.replace('radio', '');
            var shippingTime = shippingOptionData[shippingOptionData.length - 2].innerHTML;
            var shippingCost = shippingOptionData[shippingOptionData.length - 1].innerHTML;
            return {
                service: shippingService,
                time: shippingTime,
                cost: shippingCost
            };
        });
    }, shippingDataSelector);
    console.log(JSON.stringify(productShippingInfo));
}, function () {
    casper.exit();
});

casper.run();
