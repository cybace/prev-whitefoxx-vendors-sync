var casper = require('casper').create();

var sku = casper.cli.get('sku');
var option = casper.cli.get('option');
var quantity = casper.cli.get('quantity');

var URI = 'https://www.bigbuy.eu/en/search/result?q=' + sku;

casper.start().thenOpen(URI);

casper.waitForSelector('.productCard-link', function () {
    this.capture('del/0.png');
    this.click('.productCard-link');
}, function () {
    casper.exit();
});

if (option) {
    casper.waitForSelector('select[name="Design"]', function () {
        var optionSelector = 'select[name="Design"]';
        this.evaluate(function (optionSelector, option) {
            var select = document.querySelector(optionSelector);
            var children = select.children;
            for (var i = 0; i < children.length; i++) {
                select.selectedIndex = i;
                var optionValue = select.value;
                if (optionValue == option) {
                    break;
                }
            }
            var e = document.createEvent('UIEvents');
            e.initUIEvent('change', true, true);
            select.dispatchEvent(e);
        }, optionSelector, option);
        this.capture('del/1.png');
    }, function () {
    });
}

casper.waitForSelector('.form-input.form-input--postfix.unitControl-input', function () {
    this.capture('del/2.png');
    this.sendKeys('.form-input.form-input--postfix.unitControl-input', '' + quantity, { keepFocus: true });
    this.sendKeys('.form-input.form-input--postfix.unitControl-input', casper.page.event.key.Enter , { keepFocus: true });
}, function () {
    casper.exit();
});

casper.waitForSelector('.expresscart', function () {
    console.log(JSON.stringify({ inStock: 1 }));
}, function () {
    console.log(JSON.stringify({ inStock: 0 }));
});

casper.run();
