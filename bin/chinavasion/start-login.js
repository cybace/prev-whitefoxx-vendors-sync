var casper = require('casper').create();

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://secure.chinavasion.com/login/';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form input[name="user"]', username);
    this.sendKeys('form input[name="pass"]', password);
    this.click('form input[type="submit"]');
});

casper.waitWhileSelector('form input[name="pass"]', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
});

casper.run();
