var fs = require('fs');

var CookiesManager = function (cookiesFilePath) {
    this.cookiesFilePath = cookiesFilePath;
    if (!fs.existsSync(this.cookiesFilePath)) {
        fs.openSync(this.cookiesFilePath, 'w');
    }

    this.cookies = null;

    CookiesManager.prototype.set = function (cookies) {
        fs.writeFileSync(this.cookiesFilePath, JSON.stringify(cookies));
        this.setLocalCookies();
    };

    CookiesManager.prototype.setLocalCookies = function () {
        if (fs.existsSync(this.cookiesFilePath)) {
            var data = fs.readFileSync(this.cookiesFilePath);
            try {
                var cookies = JSON.parse(data);
                this.cookies = cookies;
            } catch (e) {
                this.cookies = null;
            }
        }
    };

    this.setLocalCookies();
};

module.exports = function (cookiesFilePath) {
    return new CookiesManager(cookiesFilePath);
};
