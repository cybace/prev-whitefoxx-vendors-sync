var fs = require('fs');

var CredentialsManager = function (credentialsFilePath) {
    this.credentialsFilePath = credentialsFilePath;
    if (!fs.existsSync(this.credentialsFilePath)) {
        fs.openSync(this.credentialsFilePath, 'w');
    }

    this.apiKey = null;
    this.username = null;
    this.password = null;

    CredentialsManager.prototype.set = function (credentials, cb) {
        fs.writeFileSync(this.credentialsFilePath, JSON.stringify(credentials, null, 4));
        this.setLocalCredentials();
    };

    CredentialsManager.prototype.setLocalCredentials = function () {
        if (fs.existsSync(this.credentialsFilePath)) {
            var data = fs.readFileSync(this.credentialsFilePath);
            try {
                var credentials = JSON.parse(data);
                if (credentials.apiKey) {
                    this.apiKey = credentials.apiKey;
                }
                if (credentials.username) {
                    this.username = credentials.username;
                }
                if (credentials.password) {
                    this.password = credentials.password;
                }
            } catch (e) {
                this.apiKey = null;
                this.username = null;
                this.password = null;
            }
        }
    };

    this.setLocalCredentials();
};

module.exports = function (credentialsFilePath) {
    return new CredentialsManager(credentialsFilePath);
};
