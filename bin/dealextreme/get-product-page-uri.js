var casper = require('casper').create();

var sku = casper.cli.get('sku');

var URI = 'http://www.dx.com/s/' + sku;

casper.start().thenOpen(URI, function () {
    this.click('a#content_ProductList1_rpProducts_lnkShortHeadLine1_0');
});

casper.withPopup('', function () {
    console.log(this.getCurrentUrl());
});

casper.run();
