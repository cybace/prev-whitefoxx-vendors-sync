var casper = require('casper').create();
var fs = require('fs');
var x = require('casper').selectXPath;

casper.options.waitTimeout = 60000;

var cookiesFilePath = casper.cli.get('cookies-file-path');
var uri = '' + casper.cli.get('uri');
var quantity = '' + casper.cli.get('quantity');
var countryCode = '' + casper.cli.get('countryCode');

var URI = uri;
var CART_URI = 'https://cart.dx.com/';

casper.start().thenOpen(URI, function () {
    var cookies = JSON.parse(fs.read(cookiesFilePath));
    phantom.clearCookies();
    cookies.forEach(function (cookie) {
        phantom.addCookie(cookie);
    });
});

casper.waitForSelector('#qty', function () {
    this.evaluate(function () {
        document.querySelector('#qty').value = '';
    });
    this.sendKeys('#qty', quantity, { keepFocus: true });
    this.sendKeys('#qty', this.page.event.key.Enter, { keepFocus: true });
    this.click('#btn-add-to-cart');
    this.capture('0.png');
});

casper.waitForSelector('#detailAddToCartPanel', function () {
    this.capture('1.png');
});

casper.thenOpen(CART_URI, function () {
    this.capture('2.png');
    this.captureSelector('3.png', 'button[name="checkout"]');
    this.click('button[name="checkout"]');
    // click checkout button and go to next page
});

casper.waitForSelector('#smList', function () {
    this.capture('4.png');
}, function () {
    this.capture('4.png');
});

casper.run();
