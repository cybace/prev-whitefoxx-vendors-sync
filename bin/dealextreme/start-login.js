var casper = require('casper').create();

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://passport.dx.com/?redirect=http%3A%2F%2Fwww.dx.com%2F';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form#loginForm input#email', username);
    this.sendKeys('form#loginForm input#password', password);
    this.click('form#loginForm button#login_btn');
});

casper.waitWhileSelector('form#loginForm', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
});

casper.run();
