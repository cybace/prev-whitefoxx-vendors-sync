var casper = require('casper').create();
var fs = require('fs');

var cookiesFilePath = casper.cli.get('cookies-file-path');
var cookies = JSON.parse(fs.read(cookiesFilePath));
phantom.clearCookies();
cookies.forEach(function (cookie) {
    phantom.addCookie(cookie);
});

var URI = 'https://my.dx.com/';

casper.start().thenOpen(URI);

casper.waitForSelector('.member_information', function () {
    console.log(JSON.stringify({ valid: 1 }));
}, function () {
    console.log(JSON.stringify({ valid: 0 }));
});

casper.run();
