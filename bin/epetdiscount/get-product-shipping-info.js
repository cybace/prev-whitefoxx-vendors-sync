var casper = require('casper').create();
var fs = require('fs');
var x = require('casper').selectXPath;

var cookiesFilePath = casper.cli.get('cookies-file-path');
var sku = '' + casper.cli.get('sku');
var quantity = '' + casper.cli.get('quantity');
var countryCode = '' + casper.cli.get('countryCode');
var postCode = '' + casper.cli.get('postCode');

var URI = 'https://epetdiscount.com/search?q=' + sku;

casper.start().thenOpen(URI);

casper.waitForSelector('.list-view-item__title', function () {
    this.capture('0.png');

    var cookies = JSON.parse(fs.read(cookiesFilePath));
    phantom.clearCookies();
    cookies.forEach(function (cookie) {
        phantom.addCookie(cookie);
    });

    this.click('.list-view-item__title');
});

casper.waitForSelector('#AddToCart-product-template', function () {
    this.capture('1.png');
    this.sendKeys('#Quantity', quantity);
    this.capture('2.png');
    this.click('#AddToCart-product-template');
});

casper.waitForSelector('input[name="checkout"]', function () {
    this.capture('3.png');
    this.click(x('//*[@id="shopify-section-cart-template"]/div/form/footer/div/div/input[2]'));
});

casper.waitForSelector('button[data-trekkie-id="continue_to_shipping_method_button"]', function () {
    this.capture('4.png');

    this.sendKeys('#checkout_shipping_address_first_name', 'FIRSTNAME');
    this.sendKeys('#checkout_shipping_address_last_name', 'LASTNAME');
    this.sendKeys('#checkout_shipping_address_company', 'COMPANY');
    this.sendKeys('#checkout_shipping_address_address1', 'ADDRESS');
    this.sendKeys('#checkout_shipping_address_city', 'CITY');
    var countrySelector = '#checkout_shipping_address_country';
    this.evaluate(function (countrySelector, countryCode) {
        var select = document.querySelector(countrySelector);
        Array.prototype.forEach.call(select.children, function (option, i) {
            if (option.getAttribute('data-code') == countryCode) {
                select.selectedIndex = i;
            }
        });
        var e = document.createEvent('UIEvents');
        e.initUIEvent('change', true, true);
        select.dispatchEvent(e);
    }, countrySelector, countryCode);
    this.sendKeys('#checkout_shipping_address_zip', 'SW151AA');
    this.sendKeys('#checkout_shipping_address_phone', '00', { keepFocus: true });
    this.sendKeys('#checkout_shipping_address_phone', casper.page.event.key.Enter, { keepFocus: true });

    this.capture('5.png');
});

casper.waitForSelector('.section.section--shipping-method', function () {
    this.capture('6.png');
    var productShippingData = this.evaluate(function () {
        var shippingService = document.querySelector('.radio__label__primary').innerHTML;
        shippingService = shippingService.replace(/\s/g,'');
        var shippingTime = '4-7 working days';
        var shippingCost = document.querySelector('.content-box__emphasis').innerHTML;
        shippingCost = shippingCost.replace(/\s/g,'');
        return [{
            service: shippingService,
            time: shippingTime,
            cost: shippingCost
        }];
    });
    console.log(JSON.stringify(productShippingData));
});

casper.run();
