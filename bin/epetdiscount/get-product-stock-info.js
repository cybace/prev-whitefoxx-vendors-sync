var casper = require('casper').create();
var x = require('casper').selectXPath;

var sku = '' + casper.cli.get('sku');
var quantity = '' + casper.cli.get('quantity');

var URI = 'https://epetdiscount.com/search?q=' + sku;

casper.start().thenOpen(URI);

casper.waitForSelector('.list-view-item__title', function () {
    this.capture('0.png');
    this.click('.list-view-item__title');
});

casper.waitForSelector('#AddToCart-product-template', function () {
    this.capture('1.png');
    var inStock = this.evaluate(function () {
        var addToCartBtn = document.querySelector('#AddToCart-product-template');
        return !addToCartBtn.disabled;
    });
    if (inStock) {
        console.log(JSON.stringify({ inStock: 1 }));
    } else {
        console.log(JSON.stringify({ inStock: 0 }));
    }
});

casper.run();
