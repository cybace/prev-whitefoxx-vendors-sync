var casper = require('casper').create();

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://epetdiscount.com/account/login';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form#customer_login input#CustomerEmail', username);
    this.sendKeys('form#customer_login input#CustomerPassword', password);
    this.click('input[type="submit"]');
});

casper.waitWhileSelector('form#customer_login', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
});

casper.run();
