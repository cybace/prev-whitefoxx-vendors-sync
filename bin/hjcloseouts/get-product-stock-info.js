var casper = require('casper').create();

var sku = casper.cli.get('sku');
var quantity = casper.cli.get('quantity');

var URI = 'http://www.hjcloseouts.com/?p=' + sku;

casper.start().thenOpen(URI, function () {
    var maxQuantity = this.evaluate(function () {
        return document.querySelector('.input-text.qty.text').getAttribute('max');
    });
    if (quantity <= maxQuantity) {
        console.log(JSON.stringify({ inStock: 1 }));
    } else {
        console.log(JSON.stringify({ inStock: 0 }));
    }
});

casper.run();
