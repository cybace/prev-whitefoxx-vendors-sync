var casper = require('casper').create();

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://www.hjcloseouts.com/my-account/';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form.login input#username', username);
    this.sendKeys('form.login input#password', password);
    this.click('form.login input[type="submit"]');
});

casper.waitWhileSelector('.login', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('after.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('after.png');
});

casper.run();
