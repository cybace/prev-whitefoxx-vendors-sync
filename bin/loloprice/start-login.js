var casper = require('casper').create();

casper.options.waitTimeout = 60000;

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://www.loloprice.com/login/';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form.cm-processed-form input#login_main_login', username);
    this.sendKeys('form.cm-processed-form input#psw_main_login', password, { keepFocus: true });
    this.sendKeys('form.cm-processed-form input#psw_main_login', casper.page.event.key.Enter, { keepFocus: true });
});

casper.waitWhileSelector('form.cm-processed-form input#psw_main_login', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
});

casper.run();
