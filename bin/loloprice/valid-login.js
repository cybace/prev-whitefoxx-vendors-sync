var casper = require('casper').create();
var fs = require('fs');

casper.options.waitTimeout = 60000;

var cookiesFilePath = casper.cli.get('cookies-file-path');
var cookies = JSON.parse(fs.read(cookiesFilePath));
phantom.clearCookies();
cookies.forEach(function (cookie) {
    phantom.addCookie(cookie);
});

var URI = 'https://www.loloprice.com/profiles-update/';

casper.start().thenOpen(URI);

casper.waitForSelector('.ty-account-detail', function () {
    console.log(JSON.stringify({ valid: 1 }));
}, function () {
    console.log(JSON.stringify({ valid: 0 }));
});

casper.run();
