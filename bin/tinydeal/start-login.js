var casper = require('casper').create({
    pageSettings: {
        userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5'
    }
});

var username = casper.cli.get('username');
var password = casper.cli.get('password');

var URI = 'https://www.tinydeal.com/index.php?main_page=login';

casper.start().thenOpen(URI);

casper.then(function () {
    this.sendKeys('form#logins input#login-email-address', username);
    this.sendKeys('form#logins input#login-password', password, { keepFocus: true });
    this.sendKeys('form#logins input#login-password', casper.page.event.key.Enter, { keepFocus: true });
});

casper.waitWhileSelector('form#logins', function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
}, function () {
    console.log(JSON.stringify(phantom.cookies, null, 4));
    this.capture('0.png');
});

casper.run();
