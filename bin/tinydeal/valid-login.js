var casper = require('casper').create({
    pageSettings: {
        userAgent: 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.56 Safari/536.5'
    }
});
var fs = require('fs');

var cookiesFilePath = casper.cli.get('cookies-file-path');
var cookies = JSON.parse(fs.read(cookiesFilePath));
phantom.clearCookies();
cookies.forEach(function (cookie) {
    phantom.addCookie(cookie);
});

var URI = 'https://my.tinydeal.com/summary';

casper.start().thenOpen(URI);

casper.waitForSelector('#cmr_w_nick', function () {
    console.log(JSON.stringify({ valid: 1 }));
}, function () {
    console.log(JSON.stringify({ valid: 0 }));
});

casper.run();
