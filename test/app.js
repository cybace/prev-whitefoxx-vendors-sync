var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var express = require('express');
var expressSession = require('express-session');
var fs = require('fs');
var path = require('path');
var wvs = require('../whitefoxx-vendor-sync');

var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser());
app.use(express.static(__dirname + '/public'));
app.use(expressSession({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
}));

var carts = wvs.Carts;

var bigbuy = wvs.BigBuy;
var chinavasion = wvs.Chinavasion;
// var dealextreme = wvs.DealExtreme;
// var epetdiscount = wvs.EPetDiscount;
// var hjcloseouts = wvs.HJCloseouts;
// var loloprice = wvs.LoLoPrice;
// var tinydeal = wvs.TinyDeal;

app.listen(3000, function () {
    console.log('Listening on port 3000');
});


/*******************************************************************************
 *
 * Helpers.
 *
 ******************************************************************************/

function getCartSessionIdentifier(req) {
    if (req.session.cartSessionIdentifier) {
        return req.session.cartSessionIdentifier;
    }
    req.session.cartSessionIdentifier = Date.now();
    return req.session.cartSessionIdentifier;
}

/******************************************************************************/


/*******************************************************************************
 *
 * Billing info.
 *
 ******************************************************************************/

// app.get('/billing-info', function (req, res) {
//     res.render('billing-info');
// });
// 
// app.post('/billing-info', function (req, res) {
//     var firstName = req.body.firstName;
//     var lastName = req.body.lastName;
//     var emailAddress = req.body.emailAddress;
//     var mobilePhoneNumber = req.body.mobilePhoneNumber;
//     var addressLine1 = req.body.addressLine1;
//     var addressLine2 = req.body.addressLine2;
//     var townCity = req.body.townCity;
//     var county = req.body.county;
//     var postcode = req.body.postcode;
//     var countryCode = req.body.countryCode;
//     var cardNumber = req.body.cardNumber;
//     var expiryDate = req.body.expiryDate;
//     var cvv = req.body.cvv;
// });

/******************************************************************************/


/*******************************************************************************
 *
 * Sync.
 *
 ******************************************************************************/

app.get('/bigbuy-sync', function (req, res) {
    var synced = bigbuy.synced;
    var username = bigbuy.username || 'username';
    var passwordPlaceholder = bigbuy.passwordPlaceholder || 'password';
    res.render('sync/bigbuy-sync', {
        synced: synced,
        username: username,
        passwordPlaceholder: passwordPlaceholder
    });
});

app.post('/bigbuy-sync', function (req, res) {
    var credentials = {
        username: req.body.username,
        password: req.body.password
    };
    bigbuy.sync(credentials, function (synced) {
        var username = bigbuy.username || 'username';
        var passwordPlaceholder = bigbuy.passwordPlaceholder || 'password';
        res.render('sync/bigbuy-sync', {
            synced: synced,
            username: username,
            passwordPlaceholder: passwordPlaceholder
        });
    });
});

app.get('/chinavasion-sync', function (req, res) {
    var synced = chinavasion.synced;
    var apiKeyPlaceholder = chinavasion.apiKeyPlaceholder || 'API KEY';
    res.render('sync/chinavasion-sync', {
        synced: synced,
        apiKeyPlaceholder: apiKeyPlaceholder
    });
});

app.post('/chinavasion-sync', function (req, res) {
    var credentials = {
        apiKey: req.body.apiKey
    };
    chinavasion.sync(credentials, function (synced) {
        var apiKeyPlaceholder = chinavasion.apiKeyPlaceholder || 'API KEY';
        res.render('sync/chinavasion-sync', {
            synced: synced,
            apiKeyPlaceholder: apiKeyPlaceholder
        });
    });
});

/******************************************************************************/


/*******************************************************************************
 *
 * Purchase process simulation.
 *
 ******************************************************************************/

app.get('/', function (req, res) {
    res.redirect('/storefront');
});

app.get('/storefront', function (req, res) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.header('Expires', '-1');
    res.header('Pragma', 'no-cache');

    var vendorsInfoArr = [
        [ 'bigbuy', bigbuy, '/bigbuy-sync' ],
        [ 'chinavasion', chinavasion, '/chinavasion-sync' ]
    ];
    var vendorsSynced = true;
    var vendors = [];
    vendorsInfoArr.forEach(function (vendorInfo) {
        var vendor = vendorInfo[0];
        var synced = vendorInfo[1].synced;
        if (!synced) {
            vendorsSynced = false;
        }
        var syncURI = vendorInfo[2];
        vendors.push({
            vendor: vendor,
            synced: synced,
            syncURI: syncURI
        });
    });

    var cartSessionIdentifier = getCartSessionIdentifier(req);
    var cart = carts.cart(cartSessionIdentifier);

    var sampleProducts = JSON.parse(fs.readFileSync('test/sample-data/sample-products.json', 'utf8'));
    for (var i = 0; i < sampleProducts.length; i++) {
        sampleProducts[i] = cart.currencify(sampleProducts[i]);
    }
    res.render('storefront', {
        billingInfoAdded: false,
        vendorsSynced: false,
        vendors: vendors,
        products: sampleProducts,
        cart: cart.data,
        currencies: cart.currencies,
        currency: cart.getCurrency() // todo
    });
});

// TODO cart values etc
app.post('/currency', function (req, res) {
    var currency = req.body.currency;
    var cartSessionIdentifier = getCartSessionIdentifier(req);
    var cart = carts.cart(cartSessionIdentifier);
    cart.setCurrency(currency);
    res.send(200);
});

app.post('/product-stock-info', function (req, res) {
    var vendor = req.body.vendor;
    var sku = req.body.sku;
    var option = req.body.option;
    var quantity = req.body.quantity;
    if (vendor == 'bigbuy') {
        bigbuy.getProductStockInfo(sku, option, quantity, function (stockInfo) {
            res.send(stockInfo);
        });
    } else if (vendor == 'chinavasion') {
        chinavasion.getProductStockInfo(sku, quantity, function (stockInfo) {
            res.send(stockInfo);
        });
    }
});

app.post('/product-shipping-info', function (req, res) {
    var countryCode = req.body.countryCode;
    var vendor = req.body.vendor;
    var sku = req.body.sku;
    var option = req.body.option;
    var quantity = req.body.quantity;
    if (vendor == 'bigbuy') {
        var shippingInfo = [
            {
                service: 'Standard',
                time: '4-7 working days',
                cost: '£12.00'
            },
            {
                service: 'Express',
                time: '1-2 working days',
                cost: '£24.00'
            }
        ];
        setTimeout(function () {
            res.send(JSON.stringify(shippingInfo));
        }, 5000);
//         bigbuy.getProductShippingInfo(countryCode, sku, quantity, function (shippingInfo) {
//             res.send(JSON.stringify(shippingInfo));
//         });
    } else if (vendor == 'chinavasion') {
        chinavasion.getProductShippingInfo(countryCode, sku, quantity, function (shippingInfo) {
            res.send(JSON.stringify(shippingInfo));
        });
    }
});

app.post('/add-to-cart', function (req, res) {
    var cartSessionIdentifier = getCartSessionIdentifier(req);
    var cart = carts.cart(cartSessionIdentifier);

    var vendor = req.body.vendor;
    var sku = req.body.sku;
    var uri = req.body.uri;
    var option = req.body.option || null;
    var cost = req.body.cost.replace(/[^\d.-]/g, '');
    var quantity = req.body.quantity;
    var totalCost = Number(cost)*Number(quantity);
    var product = {
        vendor: vendor,
        sku: sku,
        uri: uri,
        option: option,
        cost: {
            currency: cart.currencyCode,
            cost: cost,
            value: "todo"
        },
        quantity: quantity,
        totalCost: totalCost
    };
    cart.add(product);
    res.redirect('/storefront');
});

app.post('/remove-from-cart', function (req, res) {
    var productSku = req.body.sku;
    var cartSessionIdentifier = getCartSessionIdentifier(req);
    var cart = carts.cart(cartSessionIdentifier);
    cart.remove(productSku);
    res.redirect('/storefront');
});

app.get('/select-shipping', function (req, res) {
    var cartSessionIdentifier = getCartSessionIdentifier(req);
    var cart = carts.cart(cartSessionIdentifier);
    res.render('select-shipping', {
        cart: cart.data
    });
});

// app.get('/shopify-checkout', function (req, res) {
//     res.render('shopify-checkout');
// });
// // 
// // app.get('/purchase-status', function (req, res) {
// //     res.render('purchase-status');
// // });

// app.get('/order-tracking', function (req, res) {
//     res.render('order-tracking');
// });

/******************************************************************************/
