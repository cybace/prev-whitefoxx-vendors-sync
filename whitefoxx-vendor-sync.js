var childProcess = require('child_process');
var fs = require('fs');
var request = require('request');

var Carts = function () {
    var that = this;

    this.carts = [];
    this.cartsDirPath = './etc/carts/';

    var Cart = function (sessionIdentifier) {
        var self = this;

        this.sessionIdentifier = sessionIdentifier;
        this.data = null;
        this.currencies = [
            {
                currency: "gbp",
                symbol: "£"
            },
            {
                currency: "eur",
                symbol: "€"
            },
            {
                currency: "usd",
                symbol: "$"
            }
        ];
        this.currencyCode = 'gbp';

        Cart.prototype.setCurrency = function (currencyCode) {
            this.currencyCode = currencyCode;
            this.data.forEach(function (d) {
                console.log(JSON.stringify(d, null, 4));
            });
        };

        Cart.prototype.getCurrency = function () {
            return this.currencyCode;
        };

        Cart.prototype.currencify = function (product) {
            var sym;
            this.currencies.forEach(function (currency) {
                if (currency.currency == self.currencyCode) {
                    sym = currency.symbol;
                }
            });
            product.cost.value = sym + product.cost.cost;
            return product;
        };

        Cart.prototype.restoreFromFile = function (cartFilePath) {
            this.data = JSON.parse(fs.readFileSync(cartFilePath));
        };

        Cart.prototype.add = function (product) {
            product.cost.cost = Number(product.cost.cost) * Number(product.quantity);
            product = this.currencify(product);
            if (!this.data.length) {
                this.data.push({
                    "products": [
                        product
                    ],
                    "shipping": []
                });
            } else {
                var productAdded = false;
                this.data.forEach(function (vendor) {
                    var products = vendor.products;
                    var productsVendor = products[0].vendor;
                    if (productsVendor == product.vendor) {
                        products.forEach(function (existingProduct) {
                            if (existingProduct.sku == product.sku) {
                                existingProduct.quantity = Number(existingProduct.quantity) + Number(product.quantity);
                                existingProduct.cost.cost = Number(existingProduct.cost.cost) + Number(product.cost.cost);
                                existingProduct = self.currencify(existingProduct);
                                productAdded = true;
                            }
                        });
                        if (!productAdded) {
                            products.unshift(product);
                            productAdded = true;
                        }
                    }
                });
                if (!productAdded) {
                    this.data.push({
                        "products": [
                            product
                        ],
                        "shipping": []
                    });
                }
            }
            var cartFilePath = that.cartsDirPath + this.sessionIdentifier + '.json';
            fs.writeFileSync(cartFilePath, JSON.stringify(this.data, null, 4));
        };

        Cart.prototype.remove = function (productSku) {
            this.data.forEach(function (vendor, i) {
                var products = vendor.products;
                products.forEach(function (product, j) {
                    if (product.sku == productSku) {
                        products.splice(j, 1);
                    }
                });
                if (!products.length) {
                    self.data.splice(i, 1);
                }
            });
            var cartFilePath = that.cartsDirPath + this.sessionIdentifier + '.json';
            fs.writeFileSync(cartFilePath, JSON.stringify(this.data, null, 4));
        };
    };

    Carts.prototype.cart = function (cartSessionIdentifier) {
        var cart = null;
        for (var i = 0; i < this.carts.length; i++) {
            var c = this.carts[i];
            if (c.sessionIdentifier == cartSessionIdentifier) {
                cart = c;
            }
        }
        if (!cart) {
            cart = new Cart(cartSessionIdentifier);
            cart.data = [];
            var cartFilePath = this.cartsDirPath + cartSessionIdentifier + '.json';
            fs.writeFileSync(cartFilePath, JSON.stringify(cart.data));
            this.carts.push(cart);
        }
        return cart;
    };

    if (fs.existsSync(this.cartsDirPath)) {
        fs.readdirSync(this.cartsDirPath).forEach(function (cartFile) {
            var cartFilePath = that.cartsDirPath + cartFile;
            var data = fs.readFileSync(cartFilePath);
            try {
                var cartData = JSON.parse(data);
                var cartSessionIdentifier = cartFile.replace('.json', '');
                var restoredCart = new Cart(cartSessionIdentifier);
                restoredCart.restoreFromFile(cartFilePath);
                that.carts.push(restoredCart);
            } catch (e) {
                fs.unlinkSync(cartFilePath);
            }
        });
    } else {
        fs.mkdirSync(this.cartsDirPath);
    }
};

var BigBuy = function () {
    var that = this;

    this.cookiesFilePath = './etc/cookies/bigbuy.json';
    this.cookiesManager = require('./bin/cookies-manager')(this.cookiesFilePath);
    this.cookies = null;
    setLocalCookies();

    this.credentialsFilePath = './etc/credentials/bigbuy.json';
    this.credentialsManager = require('./bin/credentials-manager')(this.credentialsFilePath);
    this.username = null;
    this.password = null;
    this.passwordPlaceholder = null;
    setLocalCredentials();

    this.synced = false;
    if (this.cookies) {
        attemptSync(function (synced) {
            if (!synced && that.username && that.password) {
                getCookies(function (cookies) {
                    that.cookiesManager.set(cookies);
                    setLocalCookies();
                    attemptSync(function (synced) {
                        that.synced = synced;
                    });
                });
            } else {
                that.synced = synced;
            }
        });
    }

    BigBuy.prototype.sync = function (credentials, cb) {
        this.credentialsManager.set(credentials);
        setLocalCredentials();

        getCookies(function (cookies) {
            that.cookiesManager.set(cookies);
            setLocalCookies();
            attemptSync(function (synced) {
                that.synced = synced;
                console.log('bigbuy synced: ' + that.synced);
                cb(that.synced);
            });
        });
    };

    function setLocalCookies() {
        that.cookies = that.cookiesManager.cookies;
    }

    function setLocalCredentials() {
        that.username = that.credentialsManager.username;
        that.password = that.credentialsManager.password;
        if (that.password) {
            that.passwordPlaceholder = '';
            for (var i = 0; i < that.password.length; i++) {
                that.passwordPlaceholder += '*';
            }
        }
    }

    function getCookies(cb) {
        var getCookiesCmd =
            'casperjs ./bin/bigbuy/get-cookies.js' + ' ' +
            '--username=' + that.username + ' ' +
            '--password=' + that.password;
        childProcess.exec(getCookiesCmd, function (err, cookies) {
            cb(cookies);
        });
    };

    function attemptSync(cb) {
        var attemptSyncCmd =
            'casperjs ./bin/bigbuy/attempt-sync.js' + ' ' +
            '--cookies-file-path=' + that.cookiesManager.cookiesFilePath;
        childProcess.exec(attemptSyncCmd, function (err, result) {
            try {
                var syncResult = JSON.parse(result);
                if (syncResult.synced) {
                    cb(true);
                } else {
                    cb(false);
                }
            } catch (e) {
                cb(false);
            }
        });
    }

    BigBuy.prototype.getProductStockInfo = function (sku, option, quantity, cb) {
        var getProductStockInfoCmd =
            'casperjs ./bin/bigbuy/get-product-stock-info.js' + ' ' +
            '--sku=' + sku + ' ' +
            '--option=' + option + ' ' +
            '--quantity=' + quantity;
        childProcess.exec(getProductStockInfoCmd, function (err, result) {
            try {
                var stockResult = JSON.parse(result);
                console.log('is json');
                if (stockResult.inStock) {
                    cb(true);
                } else {
                    cb(false);
                }
            } catch (e) {
                console.log(result);
                cb(false);
            }
        });
    };
};

var Chinavasion = function () {
    var that = this;

    this.credentialsFilePath = './etc/credentials/chinavasion.json';
    this.credentialsManager = require('./bin/credentials-manager')(this.credentialsFilePath);
    this.apiKey = null;
    this.apiKeyPlaceholder = null;
    setLocalCredentials();

    this.synced = false;

    Chinavasion.prototype.sync = function (credentials, cb) {
        this.credentialsManager.set(credentials);
        setLocalCredentials();

        var req = {
            method: 'GET',
            url: 'https://secure.chinavasion.com/api/getCategory.php',
            json: {
                "key": that.apiKey,
                "include_content": "0"
            }
        };
        request(req, function (err, res, body) {
            if (body.error) {
                that.synced = false;
            } else {
                that.synced = true;
            }
            cb(that.synced);
        });
    };

    if (this.apiKey) {
        var credentials = {
            apiKey: this.apiKey
        };
        this.sync(credentials, function (synced) {
            that.synced = synced;
        });
    }

    function setLocalCredentials() {
        that.apiKey = that.credentialsManager.apiKey;
        if (that.apiKey) {
            that.apiKeyPlaceholder = '';
            for (var i = 0; i < that.apiKey.length; i++) {
                that.apiKeyPlaceholder += '*';
            }
        }
    }

    Chinavasion.prototype.getProductStockInfo = function (sku, quantity, cb) {
        var req = {
            method: 'GET',
            url: 'https://secure.chinavasion.com/api/getPrice.php',
            json: {
                "key": that.apiKey,
                "currency": "GBP",
                "socket": "UK",
                "products": [
                    {
                        "model_code": sku,
                        "quantity": quantity
                    }
                ],
                "shipping_country_iso2": "GB"
            }
        };
        request(req, function (err, res, body) {
            if (body.shipping.length) {
                cb(true);
            } else {
                cb(false);
            }
        });
    };

    Chinavasion.prototype.getProductShippingInfo = function (countryCode, sku, quantity, cb) {
        var req = {
            method: 'GET',
            url: 'https://secure.chinavasion.com/api/getPrice.php',
            json: {
                "key": that.apiKey,
                "currency": "GBP",
                "socket": "UK",
                "products": [
                    {
                        "model_code": sku,
                        "quantity": quantity
                    }
                ],
                "shipping_country_iso2": countryCode
            }
        };
        request(req, function (err, res, body) {
            var productShippingInfo = body.shipping;
            if (!productShippingInfo) {
                cb([{
                    'service': 'Undefined',
                    'time': 'Undefined',
                    'cost': 'Undefined'
                }]);
            } else {
                var shippingInfo = [];
                productShippingInfo.forEach(function (productShippingInfo) {
                    shippingInfo.push({
                        'service': productShippingInfo.name,
                        'time': productShippingInfo.delivery,
                        'cost': productShippingInfo.price
                    });
                });
                cb(shippingInfo);
            }
        });
    };
};

exports.Carts = new Carts;
exports.BigBuy = new BigBuy;
exports.Chinavasion = new Chinavasion;
